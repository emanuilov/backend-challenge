# Overview
A registration + payment accepting service that uses an in memory data structure (JavaScript object) to store the data. This project was created as required by an undisclosed company.

### Security notes
We could also encrypt the credit card number but we wouldn't be able to search for the /payments endpoint in an ok performing manner. In general it would be best to use Stripe or a different payments provider that handles card details and payments for us.

### Possible performance improvement
Right now two users can have the same credit card number, however If we assume that each user has a different credit card we could create a dictionary of the credit cards so we have a time complexity of 1 for the card during the /payments request

### Possible refactoring
Some of the /users validation functions could be extracted in a separate controller
If we were to use a database we would have models for the data objects that we store as part of an ORM, and we would write integration tests for that + the third party pamynets provider.

## /users - FIELDS & VALIDATIONS
### POST
- Username - alphanumeric, no spaces
- Password – min length 8, at least one upper case letter & number
- Email – email id with email format
- DoB (Date of Birth) - ISO 8601 format
- Credit Card Number – This field is optional. If given should have 16 digits.

### EXPECTED RESPONSES:

- If the request body fails to satisfy any of the basic validation checks return HTTP Status code: 400
- Reject requests if the user is under the age of 18 and return HTTP Status code: 403
- If the username has already been used reject the request and return HTTP Status code: 409
- A successful action should return HTTP Status code: 201

### GET
Consumer should be able to provide a query parameter (CreditCard=Yes/No) in his request. If “Yes” then should return Users registered with Credit Card Number.

If “No” then return Users registered without a Credit Card Number.
Without any filter should return all the Registered Users.

# Payment service with /payments endpoint for accepting payments, not saving
### POST - FIELDS & VALIDATIONS
- Credit Card Number – 16 digits
- Amount – 3 digits
### EXPECTED RESPONSES:
- If the request body fails to satisfy any of the basic validation checks return HTTP Status code: 400
- If credit card number is not registered against any Registered User return HTTP Status code: 404
- A successful payment should return HTTP Status code: 201

# Instructions
To run the project you must have Node.js installed.
Then within the project directory, run the following commands:
```
npm i
npm start
```
This will start an HTTP server open at port 3000, accessible at the following URL:
```
http://localhost:3000
```

### To run the unit tests run:
```
npm run-script test
```

### Dev commands
```
npm run-script dev
# runs a server that refreshes with every filesystem change an auto refreshing server

npm run-script test:watch
# automatically reruns the tests whenever you change the code
```