const express = require('express');
const bodyParser = require('body-parser');

const User = require('./routes/User');
const Payments = require('./routes/Payments');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

User(app);
Payments(app);

module.exports = app;