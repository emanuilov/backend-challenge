const { makePayment, validateCardNumber, validateAmount } = require('../controllers/Payments');

const { body, validationResult } = require('express-validator');

module.exports = (app) => {
	app.post('/payments',
		body('Credit Card Number').notEmpty().custom(value => validateCardNumber(value)),
		body('Amount').notEmpty().custom(value => validateAmount(value)),
		(req, res, next) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(400).json({ errors: errors.array() });
			}

			if(makePayment(req.body['Credit Card Number'], req.body['Amount'])){
				res.sendStatus(201);
			} else {
				res.sendStatus(404);
			}
		} catch (err) {
			next(err);
		}
	})
};
