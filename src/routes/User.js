const { body, check, validationResult } = require('express-validator');
const Duration = require("duration");

const datastore = require('../datastore/Datastore');
const User = require('../models/User');
const { validateCardNumber } = require('../controllers/Payments');

module.exports = (app) => {
	app.post('/users', 
		body('Username').notEmpty().custom(value => /^[A-Za-z0-9]*$/.test(value)),
		body('Password').notEmpty().custom(value => value.length >= 8 && /[A-Z]/.test(value) && /[0-9]/.test(value)),
		body('Email').notEmpty().isEmail().normalizeEmail(),
		body('DoB (Date of Birth)').notEmpty().isISO8601().toDate(),
		body('Credit Card Number').optional().custom(value => validateCardNumber(value)),
		(req, res, next) => {
			try {
				const errors = validationResult(req);
				if (!errors.isEmpty()) {
					return res.status(400).json({ errors: errors.array() });
				}

				if(new Duration(new Date(req.body['DoB (Date of Birth)']), new Date()).years < 18) {
					res.sendStatus(403);
					return;
				} else if(datastore.users[req.body.Username] !== undefined){
					res.sendStatus(409);
					return;
				}

				const user = new User(req.body.Username, req.body.Email, req.body['DoB (Date of Birth)'], req.body['Credit Card Number']);

				user.setPassword(req.body.Password).then(() => {
					datastore.users[req.body.Username] = user;

					res.sendStatus(201);
				}).catch(err => {
					next(err);
				});
			} catch (err) {
				next(err);
			}
	});

	app.get('/users',
		check('CreditCard').optional().custom(value => value === 'Yes' || value === 'No'),
		(req, res, next) => {
		try {
			if(req.query.CreditCard === "Yes"){
				res.json(Object.values(datastore.users).filter(user => user.isCardPresent() ? true : false));
			} else if(req.query.CreditCard === "No"){
				res.json(Object.values(datastore.users).filter(user => user.isCardPresent() ? false : true));
			} else{
				res.json(Object.values(datastore.users));
			}
		} catch (err) {
			next(err);
		}
	})
};
