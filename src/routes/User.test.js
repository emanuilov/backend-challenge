const request = require('supertest');
const app = require('../index');

const User = require('../models/User');
const datastore = require('../datastore/Datastore');
const data = require('../datastore/Datastore');
let correctRequestData = {
	Username: 'test',
	Password: '1234567A',
	Email: 'test@example.com',
	'DoB (Date of Birth)': '1999-07-15',
	'Credit Card Number': '3782822463100056'
};

beforeEach(() => {
	const username = 'testuser1'
	const user = new User(username, 'user@example.com', '2022-07-15', '3782822463100056');
	datastore.users[username] = user;
});

afterEach(() => {
	datastore.users = {};
});

describe('Create user', () => {
	test('Empty request', () => {
		return request(app).post('/users').send({}).expect(res => {
			if(res.body.errors.length < 4){
				throw 'Fail';
			}
		});
	});
	test('Wrong username format', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest.Username = '$3ab';
		return request(app).post('/users').send(dataTest).expect(400);
	});
	test('Correct username format', () => {
		return request(app).post('/users').send(correctRequestData).expect(201);
	});
	test('Wrong email format', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest.Email = 'dsa@';
		return request(app).post('/users').send(dataTest).expect(400);
	});
	test('No credit card', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest['Credit Card Number'] = undefined;
		return request(app).post('/users').send(dataTest).expect(201);
	});
	test('Short credit card number', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest['Credit Card Number'] = '12';
		return request(app).post('/users').send(dataTest).expect(400);
	});
	test('Non numerics in the credit card number', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest['Credit Card Number'] = '378282246310005a';
		return request(app).post('/users').send(dataTest).expect(400);
	});
	test('Bad birthdate', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest['DoB (Date of Birth)'] = '378282246310005a';
		return request(app).post('/users').send(dataTest).expect(400);
	});
	test('Too young', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest['DoB (Date of Birth)'] = '2022-01-01';
		return request(app).post('/users').send(dataTest).expect(403);
	});
	test('Existing account', () => {
		return request(app).post('/users').send(correctRequestData).expect(201)
		.then(res => request(app).post('/users').send(correctRequestData).expect(409));
	});	
});

describe('Fetch users', () => {
	test('With card - Yes check', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest.Username = 'testa'
		dataTest['Credit Card Number'] = undefined;
		return request(app).get('/users').query({CreditCard: 'Yes'}).send(correctRequestData)
		.then(res => request(app).get('/users').send(dataTest).expect(res => {
			if(res.body[0].Username !== 'test'){
				throw 'Fail';
			}
		}));
	});
	test('With card - No check', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest.Username = 'testa'
		dataTest['Credit Card Number'] = undefined;
		return request(app).get('/users').query({CreditCard: 'No'}).send(correctRequestData)
		.then(res => request(app).get('/users').send(dataTest).expect(res => {
			if(res.body[0].Username !== 'testa'){
				throw 'Fail';
			}
		}));
	});
	test('With card - Yes check', () => {
		const dataTest = Object.assign({}, correctRequestData);
		dataTest.Username = 'testa'
		dataTest['Credit Card Number'] = undefined;
		return request(app).get('/users').send(correctRequestData)
		.then(res => request(app).get('/users').send(dataTest).expect(res => {
			if(res.body.length !== 2){
				throw 'Fail';
			}
		}));
	});
});