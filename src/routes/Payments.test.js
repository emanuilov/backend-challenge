const request = require('supertest');
const app = require('../index');

const User = require('../models/User');
const datastore = require('../datastore/Datastore');

beforeAll(() => {
	const username = 'testuser1'
	const user = new User(username, 'user@example.com', '2022-07-15', '3782822463100056');
	datastore.users[username] = user;
});

afterAll(() => {
	datastore.users = {};
});

describe('Card Number', () => {
	test('Alphanumerical string', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': '123abaaaaaaaaaaa'}).expect(400);
	});

	test('Numerical string, insufficient length', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': '123'}).expect(400);
	});

	test('Letter only string', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': 'abcabcabcabcabca'}).expect(400);
	});

	test('Special characters in string', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': 'a#bc378282246310'}).expect(400);
	});
});

describe('Make payment', () => {
	test('Payment to a non existing card number', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': '3782822462200056'}).expect(404);
	});

	test('Payment to an existing card number', () => {
		return request(app).post('/payments').send({Amount: 123,'Credit Card Number': '3782822463100056'}).expect(201);
	});
});

describe('Payment amount', () => {
	test('Alpanumerical amount', () => {
		return request(app).post('/payments').send({Amount: 'a42','Credit Card Number': '3782822463100056'}).expect(400);
	});

	test('Leter only amount', () => {
		return request(app).post('/payments').send({Amount: 'aaa','Credit Card Number': '3782822463100056'}).expect(400);
	});

	test('Amount with special characters', () => {
		return request(app).post('/payments').send({Amount: '12#','Credit Card Number': '3782822463100056'}).expect(400);
	});

	test('Amount less than 3 characters', () => {
		return request(app).post('/payments').send({Amount: '1','Credit Card Number': '3782822463100056'}).expect(400);
	});
});
