const hash = require('./Hash');

test('Hash a string', async () => {
	return expect(typeof (await hash('test'))).toBe("string");
});