const bcrypt = require('bcrypt');

module.exports = async (string) => {
	return bcrypt.hash(string, 12);
}