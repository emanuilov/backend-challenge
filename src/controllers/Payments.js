const datastore = require('../datastore/Datastore');

exports.makePayment = (creditCardNumber, amount) => {
	usernames = Object.keys(datastore.users);
	let wasPaymentMade = false;

	for(let i = 0; i < usernames.length; i++) {
		if (datastore.users[usernames[i]].creditCardNumber === creditCardNumber) {
			wasPaymentMade = true;
			break;
		}
	}

	return wasPaymentMade;
}

exports.validateCardNumber = (number) => {
	return /^\d+$/.test(number) && number.toString().length === 16;
}

exports.validateAmount = (number) => {
	return /^\d+$/.test(number) && number.toString().length === 3
}