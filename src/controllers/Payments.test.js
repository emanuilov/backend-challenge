const { validateCardNumber, makePayment, validateAmount } = require('./Payments');

const User = require('../models/User');
const datastore = require('../datastore/Datastore');

beforeEach(() => {
	const username = 'testuser1'
	const user = new User(username, 'user@example.com', '2022-07-15', '3782822463100056');
	datastore.users[username] = user;
});

afterEach(() => {
	datastore.users = {};
});


describe('Card Number', () => {
	test('Alphanumerical string', () => {
		return expect(validateCardNumber('123abaaaaaaaaaaa')).toBe(false);
	});

	test('Numerical string, insufficient length', () => {
		return expect(validateCardNumber('123')).toBe(false);
	});

	test('Letter only string', () => {
		return expect(validateCardNumber('abcabcabcabcabca')).toBe(false);
	});

	test('Special characters in string', () => {
		return expect(validateCardNumber('a#bc378282246310')).toBe(false);
	});

	test('Real card number', () => {
		return expect(validateCardNumber('3782822463100056')).toBe(true);
	});
});

describe('Make payment', () => {
	test('Payment to a non existing card number', () => {
		return expect(makePayment('3782822462200056')).toBe(false);
	});

	test('Payment to an existing card number', () => {
		return expect(makePayment('3782822463100056')).toBe(true);
	});
});

describe('Payment amount', () => {
	test('Alpanumerical amount', () => {
		return expect(validateAmount('a42')).toBe(false);
	});

	test('Leter only amount', () => {
		return expect(validateAmount('aaa')).toBe(false);
	});

	test('Amount with special characters', () => {
		return expect(validateAmount('#aa')).toBe(false);
	});

	test('Amount less than 3 characters', () => {
		return expect(validateAmount('1')).toBe(false);
	});

	test('Right amount', () => {
		return expect(validateAmount('123')).toBe(true);
	});
});