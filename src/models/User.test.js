const User = require('./User');
const datastore = require('../datastore/Datastore');

beforeAll(() => {
	const username = 'testuser1'
	const user = new User(username, 'user@example.com', '2022-07-15', '3782822463100056');
	datastore.users[username] = user;
});

afterAll(() => {
	datastore.users = {};
});

test('Create a user', () => {
	const data = {
		username: 'user1',
		email: 'user@example.com',
		birthdate: '2022-07-15',
		creditCardNumber: '3782822463100056'
	};
	const user = new User(data.username, data.email, data.birthdate, data.creditCardNumber);

	return expect(user).toMatchObject(data);
});

test('Set a user password', () => {

	return datastore.users['testuser1'].setPassword('test').then(res => {
		expect(res).toBe(undefined);
	});
});

test('Check if card is present', () => {
	return expect(datastore.users['testuser1'].isCardPresent()).toBe(true);
});