const hash = require('../controllers/Hash');

module.exports = class User {
	constructor(username, email, birthdate, creditCardNumber){
		this.username = username;
		this.email = email;
		this.birthdate = birthdate;
		this.creditCardNumber = creditCardNumber;
	}

	isCardPresent() {
		return this.creditCardNumber ? true : false;
	}

	setPassword(password) {
		return new Promise((resolve, reject) => {

			hash(password).then(hashedPassword => {
				this.password = hashedPassword;
				resolve();
			}).catch(err => reject(err));
		});
	}
}